var $page = $('html, body');
$(".js-sale-button").click(function () {
    $page.animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 400);
    return false;
});

var timer2 = "30:00";
var interval = setInterval(function () {
    var timer = timer2.split(':');
    var minutes = parseInt(timer[0], 10);
    var seconds = parseInt(timer[1], 10);
    --seconds;
    minutes = (seconds < 0) ? --minutes : minutes;
    if (minutes < 0) clearInterval(interval);
    seconds = (seconds < 0) ? 59 : seconds;
    seconds = (seconds < 10) ? '0' + seconds : seconds;
    $('.countdown').html(minutes + ':' + seconds);
    timer2 = minutes + ':' + seconds;
}, 1000);

document.getElementById('phone').onkeydown = function (e) {
    return !(/^[А-Яа-яA-Za-z][]$/.test(e.key));  // IE > 9
}

const INPUT_NAME = $("#name");
const INPUT_PHONE = $("#phone");
INPUT_NAME.focus(function () {
    $("#hint-name").addClass('d-block');
});

INPUT_NAME.focusout(function () {
    $("#hint-name").removeClass('d-block');
});

INPUT_PHONE.focus(function () {
    $('#hint-phone').addClass('d-block');
});

INPUT_PHONE.focusout(function () {
    $("#hint-phone").removeClass('d-block');
});
