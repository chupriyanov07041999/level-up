$(document).ready(function () {

    var Lowl = $(".owl-carousel");
    let imagesList = $(".card-item");

    Lowl.owlCarousel({
        items:1,
        margin: 10,
        autoHeight: true,
        thumbs: true,
        thumbsPrerendered: true,
        dots: true,
        responsiveClass:true,
        responsive:{
            600:{
                items:2,
            },
            1000:{
                items:3,
                loop:false
            }
        }
    });

    $('.owl-carousel').on('changed.owl.carousel', function (event) {
        if (event.item.index === imagesList.length - 1) {
            $(".owl-carousel__item--next").addClass('d-none')
        } else {
            $(".owl-carousel__item--next").removeClass('d-none')
        }

        if (event.item.index === 0) {
            $(".owl-carousel__item--prev").addClass('d-none')
        } else {
            $(".owl-carousel__item--prev").removeClass('d-none')
        }

    });

    $(".owl-carousel__item--next").click(function () {
        Lowl.trigger('next.owl.carousel');
    });

    $(".owl-carousel__item--prev").click(function () {
        Lowl.trigger('prev.owl.carousel');
    });


    Lowl.trigger('owl.play', false);
});